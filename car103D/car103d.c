/*
  Example program for small3dlib -- a GTA-like game demo.

  Adapted for card10 and menu/timeline system by pippin.gimp.org

  author: Miloslav Ciz
  license: CC0 1.0
*/

#define _DEFAULT_SOURCE

#include "epicfb.h"

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <time.h>


uint16_t pixels[DISP_WIDTH * DISP_HEIGHT]; /* our frame buffer */


#define line_spacing  epicfb_line_spacing ()


/* we redefine printf to use our internal VT overlay console.
 */
#if 0
static char *sprintf_alloc (const char *format, ...)
{
  va_list ap;
  size_t needed;
  char  *buffer;
  va_start(ap, format);
  needed = vsnprintf(NULL, 0, format, ap) + 1;
  buffer = malloc(needed);
  va_end (ap);
  va_start(ap, format);
  vsnprintf(buffer, needed, format, ap);
  va_end (ap);
  return buffer;
}
#define printf(foo...) \
   do{ char *str = sprintf_alloc (foo);\
       epicfb_print (str);\
       free (str);\
   }while(0)
#else
  /* avoid using malloc */
#define printf(foo...) \
   do{ char str[512];\
       snprintf(str, 512, foo);\
       epicfb_print (str);\
   }while(0)
#endif



#define S3L_FLAT 0
#define S3L_STRICT_NEAR_CULLING 0

//#define S3L_PERSPECTIVE_CORRECTION 2 /* approximate perspective texturemapping */
#define S3L_PERSPECTIVE_CORRECTION 1 /* gives better quality and renders at 1fps slower*/

#define S3L_SORT 0
#define S3L_STENCIL_BUFFER 0
#define S3L_Z_BUFFER 2
#define S3L_PIXEL_FUNCTION drawPixel

#define S3L_RESOLUTION_X 160
#define S3L_RESOLUTION_Y  80

#include "small3dlib.h"

#include "cityModel.h"
#include "cityTexture.h"
#include "carModel.h"

#define TEXTURE_W 64
#define TEXTURE_H 64

#define MAX_VELOCITY 200
#define ACCELERATION 200
#define TURN_SPEED   20
#define FRICTION     200

S3L_Model3D models[2];

const uint8_t collisionMap[8 * 10] =
{
  1,1,1,1,1,1,1,1,
  1,1,1,1,0,0,0,1,
  1,1,1,1,0,1,0,1,
  2,2,1,0,0,0,0,3,
  1,2,1,0,1,1,3,1,
  2,0,0,0,1,1,3,3,
  1,0,1,0,0,1,1,1,
  1,0,0,0,1,1,1,1,
  1,1,1,1,1,1,1,1,
  1,1,1,1,1,1,1,1
};

S3L_Scene scene;


#define USE_SENSORS 1
#define DEV 1   // when uncommented car103d enables usb storage,
                   // and disables it on first press of select


#if USE_SDL
#undef USE_SENSORS
#define USE_SENSORS 0
#endif


uint32_t frame = 0;
uint32_t frame_no = 0;


static inline uint16_t fragment_small3d (int x, int y, int frameno, void *data)
{
  return 0;
}

static inline void setPixel(int x, int y, uint8_t red, uint8_t green, uint8_t blue)
{
  pixels[epicfb_offset(x,y)] = epicfb_pack (red,green,blue);
}


#if 0
static inline void getPixel(int x, int y, uint8_t *red, uint8_t *green, uint8_t *blue)
{
  y = S3L_RESOLUTION_Y - y - 1;
  uint16_t pixel = pixels[y * S3L_RESOLUTION_X + x];
  epicfb_unpack (pixel, red, green, blue);
}
#endif

#if USE_SDL
static int accelerometer_control = 0;
#else
static int accelerometer_control = 1;
#endif
static int debug_accelerometer = 1;

static int show_fps = 0;
static int vibrate_on_collision = 1;
static int fly = 0;
static int collisions = 1;
static int show_car = 1;
static int texture_effect = 0;
static int camera_no = 0;
static int camera_distance_factor = 3;

static inline void sampleTexture(int32_t ou, int32_t ov,
                                 uint8_t *r, uint8_t *g, uint8_t *b)
{
  if (texture_effect < 0)
  {
    *r = ou;
    *g = (ou^ov);
    *b = ov;
    return;
  }
  /* */
  uint32_t u = ou>>2;
  uint32_t v = ov>>2;

  u = S3L_clamp(u,0,CITY_TEXTURE_WIDTH - 1);
  v = S3L_clamp(v,0,CITY_TEXTURE_HEIGHT - 1);

  int32_t index = (v * CITY_TEXTURE_WIDTH + u) * 3;


  *r = cityTexture[index];
  index++;
  *g = cityTexture[index];
  index++;
  *b = cityTexture[index];

  switch (texture_effect)
  {
     case 0: break;
     case 1: /* add fixed position grainy noise */
     {
       uint32_t val = ((ou ^ ov * 149) * 1234 & 511);
       val = val & 0xff;
       *r = (*r * 9 + val) / 10;
       *g = (*g * 9 + val) / 10;
       *b = (*b * 9 + val) / 10;
       break;
     }
     case 2: /* turn grayscale */
     {
       uint32_t val = (*r+*g+*g+*b)/4;
       *r = *g = *b = val;
       break;
     }
     case 3: /* threshold */
     {
       uint32_t val = (*r+*g+*g+*b)/4;
       if (val > 127)
         *r = *g = *b = 255;
       else
         *r = *g = *b = 0;
       break;
     }
     case 4: /* tri-level threshold */
     {
       uint32_t val = (*r+*g+*g+*b)/4;
       if (val > 140)
         *r = *g = *b = 255;
       else if (val > 85)
         *r = *g = *b = 127;
       else
         *r = *g = *b = 0;
       break;
     }
  }
}

uint32_t previousTriangle = -1;
S3L_Vec4 uv0, uv1, uv2;

void drawPixel(S3L_PixelInfo *p)
{
  if (p->triangleID != previousTriangle)
  {
    const S3L_Index *uvIndices;
    const S3L_Unit *uvs;

    if (p->modelIndex == 0)
    {
      uvIndices = cityUVIndices;
      uvs = cityUVs;
    }
    else
    {
      uvIndices = carUVIndices;
      uvs = carUVs;
    }

    S3L_getIndexedTriangleValues(p->triangleIndex,uvIndices,uvs,2,&uv0,&uv1,&uv2);

    previousTriangle = p->triangleID;
  }

  uint8_t r, g, b;

  S3L_Unit uv[2];

  uv[0] = S3L_interpolateBarycentric(uv0.x,uv1.x,uv2.x,p->barycentric);
  uv[1] = S3L_interpolateBarycentric(uv0.y,uv1.y,uv2.y,p->barycentric);

  sampleTexture(uv[0],uv[1],&r,&g,&b);
  setPixel(p->x,p->y,r,g,b);
}


static inline uint8_t collision(S3L_Vec4 worldPosition)
{
  worldPosition.x /= S3L_FRACTIONS_PER_UNIT;
  worldPosition.z /= -S3L_FRACTIONS_PER_UNIT;

  uint16_t index = worldPosition.z * 8 + worldPosition.x;

  return collisionMap[index];
}

static inline void handleCollision(S3L_Vec4 *pos, S3L_Vec4 previousPos)
{
  S3L_Vec4 newPos = *pos;
  newPos.x = previousPos.x;

  if (collision(newPos))
  {
    newPos = *pos;
    newPos.z = previousPos.z;

    if (collision(newPos))
      newPos = previousPos;
  }

  *pos = newPos;
}


EpicFbFragment current_fragment = fragment_small3d;
void          *current_fragment_data = NULL;

static Menuitem main_menu[]; // to be able to special case in
//static Menuitem cam_menu[];  // menu renderer

static void cb_set_frag (EpicFbFragment fragment);
static void cb_set_frag_data (void *data);

#if 0
static void cb_show_allascii (const char *menu_entry)
{
  for (int c = ' '; c < 126; c++)
    printf ("%c", c);
  printf ("ascii\n");
}
#endif

static void validate_integers (void)
{
  if (epicfb_font_size < 5) epicfb_font_size = 5;
  if (epicfb_font_size > 23) epicfb_font_size = 23;
  if (camera_no < 0) camera_no = 0;
  if (camera_no > 2) camera_no = 2;
  if (texture_effect < -1) texture_effect = -1;
  if (texture_effect > 4)  texture_effect = 4;
}

static Menuitem main_menu[]={
  { MENU_ACTION_MENUTITLE,  "car103D"},
  { MENU_ACTION_CB,         "close menu", epicfb_set_menu, NULL},
  { MENU_ACTION_BOOLEAN,    "fps",        &show_fps},
  { MENU_ACTION_BOOLEAN,    "fly",        &fly},
  { MENU_ACTION_INTEGER,    "camera no",    (void*)&camera_no},
  { MENU_ACTION_BOOLEAN,    "vibration",  &vibrate_on_collision},
  { MENU_ACTION_BOOLEAN,    "collisions",       &collisions},
  { MENU_ACTION_BOOLEAN,    "accelerometer", &accelerometer_control},
  { MENU_ACTION_INTEGER,    "texturefilter",(void*)&texture_effect},
  { MENU_ACTION_INTEGER,    "distance",     (void*)&camera_distance_factor},
  { MENU_ACTION_BOOLEAN,    "render car",   &show_car},
  { MENU_ACTION_TERMINAL},
};

static void cb_set_frag (EpicFbFragment fragment)
{
  current_fragment = fragment;
}

static void cb_set_cam (void *data)
{
  camera_no = (size_t)data;
}

static void cb_set_effect (void *data)
{
  texture_effect = (size_t)data;
}

static void cb_set_frag_data (void *data)
{
  current_fragment_data = data;
}


static TimeEvent time_events[]={
  {0.0, cb_set_effect, (void*)2},
  {0.0, cb_set_cam,    (void*)1},
  {0.0, cb_set_frag, fragment_small3d},
  {0.0, cb_set_frag_data, NULL},
  {0.8, cb_set_effect, (void*)-1},
  {1.0, cb_set_cam,    (void*)0},
  {2.0, cb_set_frag, epicfb_frag_noise},
  {2.0, epicfb_print, "car103D\n"
                  "small3d city demo\n"
                  "by Miloslav Ciz\n\n"
                  "car10 port by\n"
                  "pippin.gimp.org\n"},
  {3.0, cb_set_frag, fragment_small3d},
  {6.0, epicfb_print, "\n\n\n\n\n\n\n"},
  {6.5, cb_set_effect, (void*)1},
  {7.0, epicfb_print, "\f"},
  {8.0, epicfb_print, "tilt left/right to turn\n"},
  {10.0, epicfb_print, "\n\n\n\n\n\n\n"},
  {11.0, epicfb_print, "tilt forward to accelerate, hold vertical to brake\n"},
  {15.5, epicfb_print, "\n\n\n\n\n\n\n"},
  {16.0,epicfb_print, "select\nfor menu\n\n"},
  {20.0,epicfb_print, "\n\n\n\n\n\n\n"},
};


int main()
{
#if DEV
  /* initialize usb storage, it gets disabled on pressing upper right,
     when sensor steering works, any keypress will disable usb
   */
#if USE_SDL==0
  //epic_usb_storage ();
#endif
#endif

#if USE_SENSORS
struct bhi160_sensor_config cfg = {0};
cfg.sample_buffer_len = 2;
cfg.sample_rate = 12;  // Hz
cfg.dynamic_range = 2; // g

int sd = 0;
 sd  = epic_bhi160_enable_sensor(BHI160_ACCELEROMETER, &cfg);
 //sdo  = epic_bhi160_enable_sensor(BHI160_ORIENTATION, &cfg);
#endif


  cityModelInit();
  carModelInit();

  models[0] = cityModel;
  models[1] = carModel;

  S3L_initScene(models,2,&scene);

  S3L_setTransform3D(1909,16,-3317,0,-510,0,512,512,512,&(models[1].transform));

  int running = 1;

  S3L_Vec4 carDirection;

  S3L_initVec4(&carDirection);

  epicfb_init ();

  int16_t velocity = MAX_VELOCITY/2;
  epicfb_queue_script (0, time_events, sizeof(time_events)/sizeof(time_events[0]));

  epicfb_menu_bg_brightness = 120;

  int frameStartT = epicfb_ticks;
  while (running) // main loop
  {
    epicfb_start_frame ();
    int frameDiff = (epicfb_ticks - frameStartT);
    frameStartT = epicfb_ticks;

#if USE_SENSORS
    struct bhi160_data_vector sensor_buf[1];
#endif

    models[1].transform.rotation.y += models[1].transform.rotation.z; // overturn the car for the rendering

    if (show_car)
      scene.models[1].config.visible = 1;
    else
      scene.models[1].config.visible = 0;

    if ((current_fragment == fragment_small3d))
    {
      S3L_newFrame();
      S3L_drawScene(scene);
    }
    else
    {
      epicfb_fill (pixels, current_fragment, frame_no, current_fragment_data);
    }

    models[1].transform.rotation.y -= models[1].transform.rotation.z; // turn the car back for the physics

  if (show_fps)
    epicfb_render_fps (pixels);


  int16_t step = (velocity * frameDiff) / 1000;
  int16_t stepFriction = (FRICTION * frameDiff) / 1000;
  int16_t friction = 0;

  int16_t stepVelocity = S3L_nonZero((ACCELERATION * frameDiff) / 1000);

  int steer = 0; /* 0 = ahead -1 = left 1 = right */
  int yaw = 0;
  int tilt = 0;
#if USE_SENSORS

  if (accelerometer_control && !epicfb_menu_active ())
  {
    epic_stream_read(sd, sensor_buf, sizeof(sensor_buf));
    tilt = 750-(sensor_buf[0].y * 900 / 16384); // transform into degrees mul10
    printf ("%i\r", tilt); /* XXX : why is this needed? it's not - this is
                                    proably traces of elf padding before RO strings were understood to be a problem. */

    if (tilt > 0)
      velocity += stepVelocity * tilt / 500;
    else
    {
       if (velocity > 0)
       {
         // braking
         velocity = velocity * 3 / 2;
       }
       else
       {
         // backing up
         velocity -= stepVelocity;
       }
    }

  yaw = (-sensor_buf[0].x * 900 / 16384); // transform into degrees mul10

  if (yaw < -50)
    steer = -1;
  else if (yaw > 50)
    steer = 1;
  }
#endif

  if (accelerometer_control && debug_accelerometer) {
    int red=0,green=0,blue=0;

    if (yaw > 50)
    {
       blue = 255;
    }
    else
    if (yaw < 50)
    {
      blue = 128;
    }
    if (tilt >= 0)
    {
       green = 255;
    }
    else
    {
       red = 255;
    }

    epicfb_rectangle (pixels, 79+yaw/10, 39-tilt/20, 5, 5, red, green, blue, 64);
  }

  epicfb_update (pixels, 0);

    int16_t stepRotation = TURN_SPEED * frameDiff * velocity  / (MAX_VELOCITY * 1000);

    if (stepRotation == 0 && S3L_abs(velocity) >= 200)
      stepRotation = 1;


#if USE_SENSORS
    if (accelerometer_control)
    {
       models[1].transform.rotation.y -= (stepRotation * yaw) / 200;
    }
#endif
    if (epicfb_events ())
      validate_integers ();

    {
     if (stepRotation < 1 && stepRotation > -1)
       stepRotation = 4;

     if (epicfb_event (BUTTON_SELECT, PRESS))
     {
       epicfb_set_menu (main_menu);
     }

    }

  if (accelerometer_control && !epicfb_menu_active ())
  {
    S3L_rotationToDirections(models[1].transform.rotation,S3L_FRACTIONS_PER_UNIT,&carDirection,0,0);

    S3L_Vec4 previousCarPos = models[1].transform.translation;

    friction = 0;

     /* transform car */
     switch (steer)
     {
       case -1:
         models[1].transform.rotation.z =
           S3L_min(S3L_abs(velocity) / 64, models[1].transform.rotation.z + 1);
         break;
       case 0:
         models[1].transform.rotation.z = (models[1].transform.rotation.z * 3) / 4;
         break;
       case 1:
         models[1].transform.rotation.z =
         S3L_max(-S3L_abs(velocity) / 64, models[1].transform.rotation.z - 1);
         break;
     }

    models[1].transform.translation.x += (carDirection.x * step ) / S3L_FRACTIONS_PER_UNIT;
    models[1].transform.translation.z += (carDirection.z * step ) / S3L_FRACTIONS_PER_UNIT;

    if (collisions && !fly)
    {
      uint8_t coll = collision(models[1].transform.translation);

      if (coll != 0)
      {
#if USE_SDL==0
         if (vibrate_on_collision)
           epic_vibra_vibrate (15);
#endif

         if (coll == 1)
         {
           handleCollision(&(models[1].transform.translation),previousCarPos);
           friction = 2;
         }
         else if (coll == 2)
         {
           // teleport the car
           models[1].transform.translation.x += 5 * S3L_FRACTIONS_PER_UNIT;
           models[1].transform.translation.z += 2 * S3L_FRACTIONS_PER_UNIT;
         }
        else
         {
           // teleport the car
           models[1].transform.translation.x -= 5 * S3L_FRACTIONS_PER_UNIT;
           models[1].transform.translation.z -= 2 * S3L_FRACTIONS_PER_UNIT;
         }
      }
    }

    if (tilt >= 0)
      velocity = S3L_max(0,velocity - stepFriction * friction);
    else
      velocity = S3L_min(0,velocity + stepFriction * friction);
    }

    switch (camera_no)
    {
      case 0:
        {
          S3L_Unit cameraDistance =
          S3L_interpolate(S3L_FRACTIONS_PER_UNIT / 2 * camera_distance_factor / 3,(camera_distance_factor * S3L_FRACTIONS_PER_UNIT) / 5,S3L_abs(velocity),MAX_VELOCITY);

          scene.camera.transform.translation.x =
          scene.models[1].transform.translation.x - (carDirection.x * cameraDistance) / S3L_FRACTIONS_PER_UNIT;

          scene.camera.transform.translation.y = S3L_FRACTIONS_PER_UNIT / 2;

          if (fly)
            scene.camera.transform.translation.y = S3L_FRACTIONS_PER_UNIT * camera_distance_factor / 2;

          scene.camera.transform.translation.z =
          scene.models[1].transform.translation.z - (carDirection.z * cameraDistance) / S3L_FRACTIONS_PER_UNIT;

          scene.camera.transform.rotation.x = -S3L_FRACTIONS_PER_UNIT / 16;
          scene.camera.transform.rotation.y = models[1].transform.rotation.y;
        }
        break;
      case 1:
        {
          scene.camera.transform.translation.x =
            scene.models[1].transform.translation.x;
          scene.camera.transform.translation.y = S3L_FRACTIONS_PER_UNIT * camera_distance_factor / 2;
          scene.camera.transform.translation.z =
            scene.models[1].transform.translation.z;

          scene.camera.transform.rotation.x = -S3L_FRACTIONS_PER_UNIT / 4;
          scene.camera.transform.rotation.y = models[1].transform.rotation.y;
        }
       break;
       case 2:
        {
          scene.camera.transform.translation.x =
            scene.models[1].transform.translation.x;
          scene.camera.transform.translation.y = S3L_FRACTIONS_PER_UNIT * 3 * camera_distance_factor;
          scene.camera.transform.translation.z =
            scene.models[1].transform.translation.z;

          scene.camera.transform.rotation.x = -S3L_FRACTIONS_PER_UNIT / 4;
          scene.camera.transform.rotation.y = 0;
        }
       break;
    }

    frame++;
    frame_no++;
  }

  return 0;
}


