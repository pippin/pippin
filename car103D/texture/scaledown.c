#include <stdint.h>
#include <stdio.h>
#include "cityTexture.h"

#define new_width  128
#define new_height 128

int main()
{
  printf (
"#ifndef CITY_TEXTURE_H\n"
"#define CITY_TEXTURE_H\n"
"\n");

 printf ("#define CITY_TEXTURE_WIDTH  %i\n", new_width);
 printf ("#define CITY_TEXTURE_HEIGHT %i\n", new_height);

 printf ("const uint8_t cityTexture[%i] = {\n", new_width * new_height * 3);
 for (int y = 0; y < new_height; y++)
 for (int x = 0; x < new_width; x++)
 {
   int old_x = x*2;
   int old_y = y*2;
   int offset = (old_y * (CITY_TEXTURE_WIDTH) + old_x) * 3;
   for (int c = 0; c < 3; c++)
     printf ("%i,", cityTexture[offset+c]);
 }


 printf ("};\n");
 printf ("#endif\n");


  return 0;
}
