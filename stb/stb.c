/*
 * Copyright 2019 Øyvind Kolås <pippin@gimp.org>
 */

#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "epicfb.h"

static const char *extensions[]={
   ".gif", ".GIF", NULL
};
static float slide_seconds = 5.0f;
static int   slideshow = 0;
static int   fit = 0;

#define STBI_NO_STDIO
//#define STBI_ONLY_PNG
//#define STBI_ONLY_JPEG
#define STBI_ONLY_GIF
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"


static int img_no = 0;
static int slide_start = 0;

static uint16_t pixels[DISP_WIDTH * DISP_HEIGHT];

static int quit = 0;
static int file_size = 0;

static int read_cb (void *user, char *data, int size)
{
#if USE_SDL
  FILE *file = user;
  return fread (data, 1, size, file);
#else
  int fd = (int)user;
  return epic_file_read (fd, data, size);
#endif
}
static void skip_cb (void *user, int n)
{
#if USE_SDL
  FILE *file = user;
  long pos = ftell (file);
  fseek (file, pos + n, SEEK_SET);
#else
  int fd = (int)user;
  int pos = epic_file_tell (fd);
  epic_file_seek (fd, pos + n, SEEK_SET);
#endif
}

static int eof_cb (void *user)
{
#if USE_SDL
  FILE *file = user;
  long pos = ftell (file);
#else
  int fd = (int)user;
  int pos = epic_file_tell (fd);
#endif
  if (pos >= file_size)
    return 1;
  return 0;
}

static stbi_io_callbacks clbk = {read_cb, skip_cb, eof_cb};



static char *stb_active_path = NULL;
static uint8_t *stb_buffer = NULL;
static int stb_w = -1;
static int stb_h = -1;
static int stb_c = 0;

static void stb_clear_cache (void)
{
  if (stb_active_path)
    free (stb_active_path);
  stb_active_path = NULL;
  if (stb_buffer)
    free (stb_buffer);
  stb_buffer = NULL;
}

void epicfb_stb (uint16_t   *pixels,
                 int         x0,
                 int         y0,
                 int         w,
                 int         h,
                 uint8_t     opacity,
                 int         filter,
                 int         cache,
                 const char *path)
{
  uint8_t *buffer = NULL;
  int imgw = 0, imgh = 0, c = 0;

  if (!cache)
  {
    stb_clear_cache ();
  }

  if (cache)
  {
    if (stb_active_path)
    {
     if (!strcmp (stb_active_path, path))
      {
        buffer = stb_buffer;
        stb_buffer = NULL;
        free (stb_active_path);
        stb_active_path = NULL;
        imgw = stb_w;
        imgh = stb_h;
        c  = stb_c;
      }
      else
      {
        stb_clear_cache ();
      }
    }
  }

  if (!buffer)
  {

#if USE_SDL
    FILE *f = fopen (path, "rb");
    if (!f)
      return;
    fseek (f, 0, SEEK_END);
    file_size = ftell (f);
    fseek (f, 0, SEEK_SET);
#else
    int f = epic_file_open (path, "rb");
    if (f<0)
      return;
    epic_file_seek (f, 0, SEEK_END);
    file_size = epic_file_tell (f);
    epic_file_seek (f, 0, SEEK_SET);
#endif
    buffer  = stbi_load_from_callbacks (&clbk, (void*)f, &imgw, &imgh, &c, 0);

#if USE_SDL
    fclose (f);
#else
    epic_file_close (f);
#endif
    if (!buffer)
    {
      return;
    }
  }
  epicfb_blit (buffer, imgw, imgh, c, pixels,
               x0, y0, w, h, opacity, filter);

  if (cache)
  {
    stb_active_path = strdup (path);
    stb_buffer = buffer;
    stb_w = imgw;
    stb_h = imgh;
    stb_c = c;
  }
  else
  {
    free (buffer);
  }
}

#ifdef STBI_ONLY_GIF

static stbi__context s;
unsigned char *result = 0;

#if USE_SDL
static FILE *gf = NULL;
#else
static int gf = -1;
#endif

char    *gif_active_path = NULL;
uint8_t *gifbuf = NULL;
  stbi__gif g;

int frameno = 0;

static void epicfb_stb_gif_stop (void);
static int epicfb_stb_gif_init (const char *path)
{
  if (gif_active_path)
  {
    epicfb_stb_gif_stop ();
  }
  frameno = 0;
#if USE_SDL
  gf = fopen (path, "rb");
  if (!gf)
    return -1;
  stb_w = -1;
  stb_h = -1;

  fseek (gf, 0, SEEK_END);
  file_size = ftell (gf);
  fseek (gf, 0, SEEK_SET);
#else
  gf = epic_file_open (path, "rb");
  if (gf<0)
    return -1;

  epic_file_seek (gf, 0, SEEK_END);
  file_size = epic_file_tell (gf);
  epic_file_seek (gf, 0, SEEK_SET);
#endif
  memset (&s, 0, sizeof (s));
  memset (&g, 0, sizeof (g));
  stbi__start_callbacks (&s, &clbk, (void*)gf);

  gif_active_path = strdup (path);
  return 0;
}

static int epicfb_stb_gif_load_frame ()
{
  int c;
  if (!gif_active_path)
    return -1;

  gifbuf = stbi__gif_load_next (&s, &g, &c, 4, 0);
  if (gifbuf == (uint8_t*) &s)
    {
      gifbuf = NULL;
      epicfb_stb_gif_stop ();
      return -1;
    }
  if (stb_w < 0) stb_w = g.w;
  if (stb_h < 0) stb_h = g.h;

  frameno++;

  return g.delay;
}

static void epicfb_stb_gif_stop (void)
{
  if (!gif_active_path)
    return;
  free (gif_active_path);
  gif_active_path = NULL;
#if USE_SDL
  fclose (gf);
  gf = NULL;
#else
  epic_file_close (gf);
  gf = -1;
#endif
  if (g.out)
  {
    free (g.out);
    g.out = NULL;
  }
  if (g.history)
  {
    free (g.history);
    g.history = NULL;
  }
  if (g.background)
  {
    free (g.background);
    g.background = NULL;
  }
}

static void epicfb_stb_gif_blit (uint16_t *pixels,
                                 int       x0,
                                 int       y0,
                                 int       w,
                                 int       h,
                                 uint8_t   opacity,
                                 int       filter)
{
  if (!gifbuf)
    return;
  epicfb_blit (gifbuf, stb_w, stb_h, 4, pixels,
               x0, y0, w, h, opacity, filter);
}

#endif

static void liberate_resources (void)
{
  stb_clear_cache ();
#ifdef STBI_ONLY_GIF
  if (gif_active_path)
    epicfb_stb_gif_stop ();
#endif
}




/****************************/


char *path = NULL;

static void cb_toggle_slideshow (void *data)
{
  slideshow = !slideshow;
  slide_start = epicfb_ticks_live();
}

static void cb_next_file (void *data)
{
  liberate_resources ();
  img_no ++;
  slide_start = epicfb_ticks_live();
  if (path)
    free (path);
  path = epicfb_path_no (img_no, extensions);
  if (!path)
  {
    img_no = 0;
    path = epicfb_path_no (img_no, extensions);
  }
}

static void cb_prev_file (void *data)
{
  liberate_resources ();

  img_no --;
  slide_start = epicfb_ticks_live ();

  if (img_no < 0){
    img_no = 0;
    while (epicfb_path_no (img_no + 1, extensions))
      img_no ++;
  }
  if (path)
    free (path);
  path = epicfb_path_no (img_no, extensions);
}

int main(int argc, char *argv[])
{
  path = epicfb_path_no (img_no, extensions);
  epicfb_init ();

  while (!quit)
  {
    epicfb_start_frame ();

    int delay = 0;
    int x0 = 0;
    int y0 = 0;
    int  w = 160;
    int  h = 0;
    int opacity = 255;
    int filter  = 1;
    int cache   = 1;

    switch (fit)
    {
      case 0: // width
        break;
      case 1: // height
        w = 0;
        h = 80;
        break;
      case 2: // 1:1
        w = 0;
        h = 0;
        break;
    }

    if (path)
    {
#ifdef STBI_ONLY_GIF
      if (strstr (path, ".gif") ||
          strstr (path, ".GIF"))
      {
repeat:
        if (!gif_active_path ||
            (gif_active_path && strcmp (path, gif_active_path)))
        {
          if (epicfb_stb_gif_init (path) == 0)
          {
            delay = epicfb_stb_gif_load_frame ();
            if (delay >= 0)
            {
              epicfb_stb_gif_blit (pixels, x0, y0, w, h, opacity, filter);
            }
          }
        }
        else
        {
          if (gif_active_path)
          {
            delay = epicfb_stb_gif_load_frame ();
            if (delay >= 0)
            {
              epicfb_stb_gif_blit (pixels, x0, y0, w, h, opacity, filter);
            }
            else
            {
              goto repeat; /* we loop GIFs infinitely */
            }
          }
        }
      }
      else
#endif
      {
        epicfb_stb (pixels, x0, y0, w, h, opacity, filter, cache, path);
      }
    }
    else
    {
      if (img_no == 0)
        return 0;
      img_no = 0;
      path = epicfb_path_no (img_no, extensions);
    }
    epicfb_update (pixels, EPICFB_CLEAR);

    if (delay>=0) /* only gifs set it non-0 */
    {
      int frame_start = epicfb_ticks;
      int now = epicfb_ticks_live ();
      epicfb_sleep (delay - (now-frame_start));
    }

    if (slideshow)
    {
      if (epicfb_ticks_live() > slide_start + slide_seconds * 1000)
        cb_next_file (NULL);
    }

    epicfb_events ();

    if (epicfb_event (BUTTON_RIGHT, LONG_PRESS))
      cb_toggle_slideshow (NULL);

    if (epicfb_event (BUTTON_RIGHT, PRESS))
      cb_next_file (NULL);

    if (epicfb_event (BUTTON_LEFT, PRESS))
      cb_prev_file (NULL);

    if (epicfb_event (BUTTON_SELECT, PRESS))
    {
      fit++;
      if (fit > 2) fit = 0;
    }

    if (epicfb_event (BUTTON_QUIT, PRESS))
    {
      quit = 1;
    }
  }
  liberate_resources (); /* to please valgrind */
  return 0;
}
