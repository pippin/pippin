/*
 * Copyright (c) 2019 Øyvind Kolås <pippin@gimp.org>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#ifndef EPICFB_H
#define EPICFB_H

#if USE_SDL
  #define DISP_WIDTH  160
  #define DISP_HEIGHT 80
#else
  #include "epicardium.h"
#endif

#include <stdint.h>

void epicfb_init (void);
unsigned int epicfb_ticks_live (void);
void epicfb_sleep (int ms);

/* render a rectangle
 */
void epicfb_rectangle (uint16_t *fb,
                       int     x,
                       int     y,
                       int     width,
                       int     height,
                       uint8_t red,
                       uint8_t green,
                       uint8_t blue,
                       uint8_t alpha);

/* render a string on top of frame buffer,
 */
int epicfb_string (uint16_t   *fb,
                   int         x,
                   int y,
                   const char *str,
                   int         font_size,
                   uint8_t     red,
                   uint8_t     green,
                   uint8_t     blue,
                   uint8_t     alpha);

void epicfb_png (uint16_t *pixels,
                 int      x0,
                 int      y0,
                 int      w,
                 int      h,
                 uint8_t  opacity,
                 int      filter,
                 int      cache,
                 const char *path);

void epicfb_png_last_dim (int *width,
                          int *height);

/* pixelformat handling for byteswapped 565 */

void epicfb_start_frame (void);
static inline uint16_t epicfb_pack (uint8_t red,
                                    uint8_t green,
                                    uint8_t blue)
{
  uint32_t c = (red >> 3) << 11;
           c |= (green >> 2) << 5;
           c |= blue >> 3;
#if USE_SDL
  return c;
#else
  return (c>>8)|(c<<8); /* swap bytes */
#endif
}


static inline void epicfb_unpack (uint16_t pixel,
                                  uint8_t *red,
                                  uint8_t *green,
                                  uint8_t *blue)
{
  uint16_t byteswapped;
#if USE_SDL
  byteswapped = pixel;
#else
  byteswapped = (pixel>>8)|(pixel<<8);
#endif
  *blue   = (byteswapped & 31)<<3;
  *green = ((byteswapped>>5) & 63)<<2;
  *red   = ((byteswapped>>11) & 31)<<3;
}

static inline uint8_t lerp_u8 (uint8_t v0, uint8_t v1, uint8_t dx)
{
  return (((((v0)<<8) + (dx) * ((v1) - (v0))))>>8);
  //return (((v0) * (255-dx) + (v1) * dx)>>8); // is this faster?
}

/* blends the specified 8bit r,g,b,a values over the
   existing pixels color, warning not using linear like we should
 */
static inline uint16_t epicfb_pack_blend (uint16_t pixel,
                                          uint8_t  red,
                                          uint8_t  green,
                                          uint8_t  blue,
                                          uint8_t  alpha)
{
  uint8_t rgb[3];
  epicfb_unpack (pixel, &rgb[0], &rgb[1], &rgb[2]);
  rgb[0] = lerp_u8 (rgb[0], red, alpha);
  rgb[1] = lerp_u8 (rgb[1], green, alpha);
  rgb[2] = lerp_u8 (rgb[2], blue, alpha);
  return epicfb_pack (rgb[0], rgb[1], rgb[2]);
}

static inline int epicfb_offset (int x, int y)
{
#if USE_SDL
#else
  x = DISP_WIDTH  - x - 1;
  y = DISP_HEIGHT - y - 1;
#endif
  return y * DISP_WIDTH + x;
}


/* lowers the brightness of the ontents of the frame buffer if
   level < 255, increases it if above - useful for collisions
   or shading for rendering menus
*/
void epicfb_brightness (uint16_t *fb,
                        uint16_t  level);




typedef uint16_t (*EpicFbFragment)(int x, int y, int frame_no, void *data);

static inline int epicfb_rand () {
  static int g_seed = 23;
  g_seed = (214013*g_seed+2531011);
  return (g_seed>>16)&0x7FFF;
}

uint16_t epicfb_frag_noise (int x, int y, int frameno, void *data);

void epicfb_fill             (uint16_t *pixels,
                              EpicFbFragment fragfun,
                              int frameno,
                              void *data);

/* also updates ticks */
void epicfb_clear            (uint16_t *fb);

extern unsigned int epicfb_ticks; /* number of ms passed since start */

/* render an FPS overlay - that itself counts frames per second */
void epicfb_render_fps    (uint16_t *fb);





int epicfb_events (void);

extern int epicfb_long_press_delay;
extern int epicfb_press_repeat_delay;

#if USE_SDL
  #define BUTTON_LEFT_BOTTOM  1
  #define BUTTON_RIGHT_BOTTOM 2
  #define BUTTON_RIGHT_TOP    4
  #define BUTTON_LEFT_TOP     8
  #define BUTTON_RESET        BUTTON_LEFT_TOP
#endif

#define BUTTON_LEFT   BUTTON_LEFT_BOTTOM
#define BUTTON_RIGHT  BUTTON_RIGHT_BOTTOM
#define BUTTON_SELECT BUTTON_RIGHT_TOP
#define BUTTON_QUIT   BUTTON_RESET

typedef enum EpicButtonTestk {
  PRESSED,
  PRESS,
  PRESS_REPEAT,
  LONG_PRESS,
  RELEASE,
} EpicButtonTest;

int epicfb_event (int button, int type);




/* font meta data, the console and menu's use the global integer
 * variables for rendering
 */
extern int epicfb_font_size;             // default 13 - no rescaling
extern int epicfb_line_spacing_percent;  // default 92
extern int epicfb_menu_bg_brightness;    // default 127

/* returns the used pixel advance for each font_size */
static inline int epicfb_font_advance (int font_size)
{
  return 8 * font_size / 13 + 1;
}

int epicfb_line_spacing_px (void); /* get current line spacing in px */

/* minimalistic auto scrolling terminal, can be rendered on top
   of a frame buffer with this:  */
void epicfb_render_vt        (uint16_t *fb);

/* adds a string characters to the epicfb vt, special recogniced codes are:
    \n  \r  and \f  ,  newline,  carraige return and blank line, and form feed
*/
void epicfb_print            (const char *str);


typedef void (*MenuCB)       (void *data);
typedef char *(*MenuLabelCB) (void *data);

typedef enum MenuEntry {
  MENU_ACTION_MENUTITLE,
  MENU_ACTION_BOOLEAN,
  MENU_ACTION_INTEGER,
  MENU_ACTION_CB,
  MENU_ACTION_SHOW_STRING,
  MENU_ACTION_COMPUTE_STRING,
  MENU_ACTION_TERMINAL,
} MenuEntry;

typedef struct Menuitem
{
  uint8_t      action;
  const char  *label;
  void        *data;
  const char  *data2;
} Menuitem;

void epicfb_queue_cb      (int ms, MenuCB cb, void *data, MenuCB finalize);
int  epicfb_menu_active   (void);
void epicfb_set_menu      (Menuitem *new_menu);

void epicfb_dirty (void);
int  epicfb_isdirty (void);

typedef struct TimeEvent
{
  float       time; /* at what time this message gets executed */
  void       *cb;
  void       *cb_data;
} TimeEvent;


void epicfb_queue_script (int ms, TimeEvent *events, int n_events);
void epicfb_iterate_script (void);

void epicfb_render_menu (uint16_t *fb);

/* upload the framebuffer to display, and update button state
 * for event processing
 */
#define EPICFB_VT        1
#define EPICFB_MENU      2
#define EPICFB_CLEAR     4
#define EPICFB_DEFAULT_FLAGS  (EPICFB_CLEAR | EPICFB_VT | EPICFB_MENU)

void epicfb_flip (uint16_t *fb);

static inline void epicfb_update (uint16_t *fb, int flags)
{
  if (flags == 0)
    flags = EPICFB_DEFAULT_FLAGS;

  if (flags & EPICFB_VT)
    epicfb_render_vt   (fb);
  if (flags & EPICFB_MENU)
    epicfb_render_menu (fb);

   epicfb_flip (fb);

  if (flags & EPICFB_CLEAR)
    epicfb_clear (fb);
}

char *epicfb_path_no (int no, const char **extensions);

uint8_t *epicfb_file_load(const char *path, int *data_size);
void epicfb_blit (const uint8_t *src,
                  int       srcw,
                  int       srch,
                  int       c,
                  uint16_t *dst,
                  int      x0,
                  int      y0,
                  int      w,
                  int      h,
                  uint8_t  opacity,
                  int      filter);

#endif
