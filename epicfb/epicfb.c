/*
 * Copyright (c) 2019 Øyvind Kolås <pippin@gimp.org>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#define _DEFAULT_SOURCE

#include <stdint.h>

#if USE_SDL
#include <SDL.h>
#include <dirent.h>
#include <unistd.h>
#else
#include "epicardium.h"
#endif

#include "epicfb.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

unsigned int epicfb_ticks     = 0;
int epicfb_long_press_delay   = 600;
int epicfb_press_repeat_delay = 400;
int epicfb_menu_bg_brightness = 127;

static uint64_t epicfb_time_start;
static uint64_t epicfb_time_now = 0;

static int32_t epicfb_state = 0;
static int32_t epicfb_prev_state = 0;


static int frame_changed = 1;

static int menu_was_active = 0;
static int long_press_mask = 0;
static int prev_long_press_mask = 0;


#if USE_SDL
    #define  epic_rtc_get_milliseconds   SDL_GetTicks
#endif

#if USE_SDL
  SDL_Window *window;
  SDL_Renderer *renderer;
  SDL_Texture *textureSDL;
  SDL_Surface *screenSurface;
  SDL_Event event;
#endif

void epicfb_init (void)
{
#if USE_SDL

  window = SDL_CreateWindow("epicfb", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, DISP_WIDTH * 4, DISP_HEIGHT * 4, SDL_WINDOW_SHOWN);
  renderer = SDL_CreateRenderer(window,-1,0);
  textureSDL = SDL_CreateTexture(renderer,SDL_PIXELFORMAT_RGB565, SDL_TEXTUREACCESS_STATIC, DISP_WIDTH, DISP_HEIGHT);
  screenSurface = SDL_GetWindowSurface(window);
#else
#endif
}

unsigned int epicfb_ticks_live (void)
{
  if (epicfb_time_start == 0)
  {
    epicfb_time_start = epic_rtc_get_milliseconds ();
  }
  epicfb_time_now = epic_rtc_get_milliseconds ();
  epicfb_ticks = (epicfb_time_now - epicfb_time_start);
  return epicfb_ticks;
}

void epicfb_start_frame (void)
{
  epicfb_ticks_live ();

  epicfb_prev_state = epicfb_state;
  prev_long_press_mask = long_press_mask; // we use one frame delayed mask update,
                                          // to permit registering long press after regular
  menu_was_active = 0;
  long_press_mask = 0;
}

void epicfb_clear (uint16_t *fb)
{
  memset (fb, 0, DISP_WIDTH*DISP_HEIGHT * 2);
  frame_changed = 0;
}

void epicfb_brightness (uint16_t *fb,
                        uint16_t  level) /* 255 = nop < darken > brighten  */
{
  /* reduce brightness, by readback and setback in one loop */
  for (int o = 0; o < DISP_WIDTH * DISP_HEIGHT; o++)
    {
      uint8_t rgb[3];
      epicfb_unpack (fb[o], &rgb[0], &rgb[1], &rgb[2]);
      for (int c=0;c<3;c++)
      {
        int newlevel = (rgb[c]*level)/255;
        if (newlevel > 255) newlevel = 255;
        rgb[c]=newlevel;
      }
      fb[o]=epicfb_pack (rgb[0], rgb[1], rgb[2]);
    }
}

#include "sgi-font.h"

static int fontbit_is_set (int ascii, int x, int y)
{
  if (sgi_font[ascii-' '][y] & (1<<(7-x)))
    return 1;
  return 0;
}

static void epicfb_glyph (uint16_t *pixels,
                          int     x,
                          int     y,
                          int     size, /* 13 is 1:1   */
                          uint8_t ascii,
                          uint8_t red,
                          uint8_t green,
                          uint8_t blue,
                          uint8_t alpha)
{
  if (size == 13 && alpha == 255)
  {
    uint16_t pixel = epicfb_pack (red, green, blue);
    for (int v = 0; v < 13; v++)
      for (int u = 0; u < 8; u++)
      {
        if (fontbit_is_set(ascii, u, v))
        {
          int rx = (x+u);
          int ry = (y-v);
          if (rx >= 0 && ry >= 0 &&
              rx < DISP_WIDTH && ry < DISP_HEIGHT)
            pixels[epicfb_offset(rx,ry)] = pixel;
        }
      }
  }
  else if (size >= 13) /* nearest neighbor when scaling up */
  {
    for (int v = 0; v < size; v++)
      for (int u = 0; u < 8 * size / 13; u++)
      {
        if (fontbit_is_set(ascii, u*13/size, v * 13 / size))
        {
          int rx = x+u;
          int ry = y-v;
          if (rx >= 0 && ry >= 0 &&
              rx < DISP_WIDTH && ry < DISP_HEIGHT)
          {
            int o = epicfb_offset (rx, ry);
            pixels[o] = epicfb_pack_blend (pixels[o], red, green, blue, alpha);
          }

        }
      }
  }
  else
  {
    int nom = 3;
    int denom = 3;
    uint8_t rgb[3] = {red,green,blue};

    nom = 13;
    denom = size;

    for (int v = 0; v < 13 * denom/nom; v++)
    for (int u = 0; u < 8 * denom/nom; u++)
    {
       int rx = x+u;
       int ry = y-v;

       if (rx >= 0 && ry >= 0 &&
           rx < DISP_WIDTH && ry < DISP_HEIGHT)
         {
/* low effort resampling; useful for games - but actual
designed tiny fonts (with aa) are better for ui, this gives ability to
vary some sizes without bre */

#define BITAT(lx,ly)   \
  fontbit_is_set(ascii, u*nom/denom+ly, v*nom/denom+lx)
            int bitsum = BITAT(0,0) +
                         BITAT(0,1) +
                         BITAT(1,1) +
                         BITAT(1,0);
#undef BITAT

             int o = epicfb_offset (rx, ry);
             if (bitsum == 4)
             {
               pixels[o] = epicfb_pack (rgb[0], rgb[1], rgb[2]);
             }
             else // alpha blend
             {
               uint8_t drgb[3];
               epicfb_unpack (pixels[o], &drgb[0], &drgb[1], &drgb[2]);
               for (int c= 0; c < 3; c++)
                 drgb[c] = ((int)drgb[c] * (4-bitsum) + (int)rgb[c] * bitsum) >> 2;

               pixels[o] = epicfb_pack (drgb[0], drgb[1], drgb[2]);
             }
         }
    }
  }
}

int epicfb_string (uint16_t   *fb,
                   int         x,
                   int         y,
                   const char *str,
                   int         font_size,
                   uint8_t     red,
                   uint8_t     green,
                   uint8_t     blue,
                   uint8_t     alpha)
{
  int advance = epicfb_font_advance (font_size);
  int dx = x;

  for (int no = 0; str[no]; no++)
  {
    epicfb_glyph (fb, dx, y, font_size, str[no],
                  red, green, blue, alpha);

    dx += advance;
  }
  return dx;
}

uint16_t epicfb_frag_noise (int x, int y, int frameno, void *data)
{
  int val = 96 + epicfb_rand () % 64;
  return epicfb_pack (val, val, val);
}

#if 0
static inline int fragment_ripple (int x, int y, int foo)
{
  int u, v;
  u = x - DISP_WIDTH/2;
  v = y - DISP_HEIGHT/2;
  return (((u * u + v * v)+foo) % (2000) < 1000) * 255;
}

static inline int fragment_ripple_interference (int x, int y,int foo)
{
  return fragment_ripple(x-(foo-256),y,0)^ fragment_ripple(x+(foo-256),y,0);
}
#endif

void epicfb_fill (uint16_t      *pixels,
                  EpicFbFragment fragfun,
                  int            frameno,
                  void          *data)
{
  int offset = 0;
  for (int y = 0; y < DISP_HEIGHT; y++)
  for (int x = 0; x < DISP_WIDTH; x++, offset++)
    pixels[offset] = fragfun (x, y, frameno, data);
}


#define MAX_VT_LINES 40

static char *vt_line[MAX_VT_LINES]={0,};
static int vt_lines = 0;

int epicfb_font_size = 13;
int epicfb_line_spacing_percent = 92;

int epicfb_line_spacing_px (void)
{
  return epicfb_font_size * epicfb_line_spacing_percent / 100;
}

#define line_spacing  epicfb_line_spacing_px ()

static void epicfb_vt_trim (void)
{
  int i;
  free (vt_line[0]);
  for (i = 0; i < vt_lines;i++)
    vt_line[i] = vt_line[i+1];
  vt_line[i] = NULL;
  vt_lines--;
}

void epicfb_print (const char *str)
{
  char line[256]="";
  if (vt_line[vt_lines])
    strcpy (line, vt_line[vt_lines]);

  int advance = epicfb_font_advance(epicfb_font_size);
  for (const char *p= str; *p; p++)
    {
      if (*p == '\f')  /* from-feed, clears scroll back */
      {
        while (vt_lines > 0)
          epicfb_vt_trim ();
        line[0]=0;
      } else
      if (*p == '\r') /* carriage return, also erases contents of line */
      {
        line[0]=0;
        if (vt_line[vt_lines])
          free (vt_line[vt_lines]);
        vt_line[vt_lines] = strdup(line);
      } else
      if (*p == '\n') /* newline */
      {
        if (vt_line[vt_lines])
          free (vt_line[vt_lines]);
        vt_line[vt_lines] = strdup(line);
        vt_lines++;
        line[0]=0;
        vt_line[vt_lines] = strdup(line);

        if (vt_lines >= MAX_VT_LINES-2 ||
            vt_lines >= DISP_HEIGHT / line_spacing + 2)
        {
          epicfb_vt_trim ();
        }

      } else {
        if (strlen(line)<240) // colwrap prevents this, but lets be paranoid
        {
          line[strlen(line)+1]=0;
          line[strlen(line)]= *p;
        }

        if (vt_line[vt_lines])
          free (vt_line[vt_lines]);
        vt_line[vt_lines] = strdup(line);

        if (strlen (line)>=DISP_WIDTH / advance  ) /* screen wrap */{
          vt_lines++;
          line[0]=0;
          vt_line[vt_lines] = strdup(line);

          if (vt_lines >= MAX_VT_LINES-2 ||
              vt_lines >= DISP_HEIGHT / line_spacing + 2)
          {
            epicfb_vt_trim ();
          }

        }
      }
    }
  epicfb_dirty();
}

void epicfb_render_vt (uint16_t *fb)
{
    int y = 1+line_spacing;
    for (int i = 0; i < vt_lines + 1; i++)
    {
       if (vt_line[i])
       epicfb_string (fb, 2, y, vt_line[i], epicfb_font_size, 255, 255, 100, 255);
       y += line_spacing;
    }
    {
      if (y - line_spacing >= DISP_HEIGHT)
      {
        epicfb_vt_trim ();
      }
    }
}


Menuitem  *menu = NULL;
static int menu_entry_active = 0;
static int menu_entry = 0;
static int menu_offset = 0;


void epicfb_render_menu (uint16_t *fb)
{
  if (!menu)
    return;

  for (int i = 0; menu[i].action != MENU_ACTION_TERMINAL; i++)
  {
    char buf[64];
    uint8_t red, green, blue, alpha;
    red = green = blue = alpha = 255;
    snprintf (buf, 64, menu[i].label);

    if (i == menu_entry && !menu_entry_active)
    {
      epicfb_string (fb, -2, line_spacing+(i+menu_offset)*line_spacing,
                     ">", epicfb_font_size, red, green, blue, alpha);
      if (line_spacing+(i+menu_offset)*line_spacing > DISP_HEIGHT)
        menu_offset--;
      if (line_spacing+(i+menu_offset)*line_spacing < line_spacing)
        menu_offset++;
    }

    switch (menu[i].action)
    {
      case MENU_ACTION_INTEGER:
        {
          int value = *(int*)(menu[i].data);
          snprintf (buf, 64, "%s%s%i%s", menu[i].label,
                menu_entry_active&&i==menu_entry?">":" ", value,
                menu_entry_active&&i==menu_entry?"<":" ");

        }
        break;

      case MENU_ACTION_BOOLEAN:
        {
          int value = *(int*)(menu[i].data);
          snprintf (buf, 64, "%s %s", menu[i].label, value?"ON":"OFF");

        if (*(int*)(menu[i].data))
          {
            blue = 200;
          }
        else
          {
            red = green = 200;
          }
        }

        break;

      case MENU_ACTION_SHOW_STRING:
        {
          const char *value = menu[i].data;
          snprintf (buf, 64, "%s%s", menu[i].label, value);
        }
        break;
      case MENU_ACTION_COMPUTE_STRING:
        {
          char *value = ((MenuLabelCB)menu[i].data)((void*)menu[i].data2);
          if (value)
          {
            snprintf (buf, 64, "%s%s", menu[i].label, value);
            free (value);
          }
        }
        break;
      case MENU_ACTION_MENUTITLE:
        blue = 64;
        break;
      default:
        break;
    }

    epicfb_string (fb, 6, line_spacing+(i+menu_offset)*line_spacing, buf, epicfb_font_size, red, green, blue, alpha);
  }
}

void epicfb_set_menu (Menuitem *new_menu)
{
  menu = new_menu;
  menu_entry = 1;
  menu_offset = 0;
  epicfb_dirty();
}

static int in_menu_events = 0;

static int epicfb_menu_events (void)
{
  int ret = 0;
  if (!menu)
    return ret;
  in_menu_events = 1;
  menu_was_active = 1;
  if (menu_entry_active) // only integers are active for now
  {
    if (epicfb_event (BUTTON_LEFT, PRESS_REPEAT))
    {
      int value = *((int*)menu[menu_entry].data);
      if ((value > 100) && (value % 10 == 0))
        value -= 10;
      else
        value -= 1;
      *((int*)menu[menu_entry].data) = value;
      ret = 1;
      epicfb_dirty();
    }
    if (epicfb_event (BUTTON_RIGHT, PRESS_REPEAT))
    {
      int value = *((int*)menu[menu_entry].data);
      if ((value > 100) && (value % 10 == 0))
        value += 10;
      else
        value += 1;
      *((int*)menu[menu_entry].data) = value;
      ret = 1;
      epicfb_dirty();
    }
    if (epicfb_event (BUTTON_SELECT, PRESS))
    {
      menu_entry_active = 0;
      epicfb_dirty();
    }
  }
  else
  {
    if (epicfb_event (BUTTON_LEFT, PRESS))
    {
      menu_entry --;
      if (menu_entry < 1){
        while (menu[menu_entry+1].action != MENU_ACTION_TERMINAL)
          menu_entry++;
      }
      epicfb_dirty();
    }
    else if (epicfb_event (BUTTON_RIGHT, PRESS))
    {
      menu_entry ++;
      if (menu[menu_entry].action == MENU_ACTION_TERMINAL)
      {
        menu_entry = 1;
        menu_offset = 0;
      }
      epicfb_dirty();
    }
    else if (epicfb_event (BUTTON_SELECT, PRESS))
    {
      epicfb_dirty();
      switch (menu[menu_entry].action)
      {
        case MENU_ACTION_CB:
          ((MenuCB)(menu[menu_entry].data))((void*)menu[menu_entry].data2);
          ret = 1;
          break;

        case MENU_ACTION_BOOLEAN:
          *((int*)menu[menu_entry].data) = !(*((int*)menu[menu_entry].data));
          ret = 1;
          break;
        case MENU_ACTION_INTEGER:
          menu_entry_active = 1;
          ret = 1;
          break;
        default:
          break;
      }
    }
  }
  in_menu_events = 0;

  return ret;
}


void epicfb_render_fps (uint16_t *fb)
{
  static int fps = 0;
  static int last_fps_time = 0;
  int time_diff = (epicfb_ticks - last_fps_time);
  static char fpsbuf[32]="";
  fps++;
  if (time_diff >= 5000)
  {
    fpsbuf[0]=0;
    last_fps_time = epicfb_ticks;
    fps = 0;
  }
  else if (time_diff >= 1000)
  {
    last_fps_time = epicfb_ticks;
    snprintf (fpsbuf, sizeof (fpsbuf), "%i", fps);
    fps = 0;
  }
  epicfb_string (fb, DISP_WIDTH-epicfb_font_size*5/2, DISP_HEIGHT-3,
                 fpsbuf, epicfb_font_size, 255, 0,0, 255);
}


void epicfb_rectangle (uint16_t *fb,
                       int       x,
                       int       y,
                       int       width,
                       int       height,
                       uint8_t   red,
                       uint8_t   green,
                       uint8_t   blue,
                       uint8_t   alpha)
{
  if (x < 0)
  {
    width += x;
    x = 0;
  }
  if (y < 0)
  {
    height += y;
    y = 0;
  }
  if (x + width >= DISP_WIDTH - 1)
  {
    width = DISP_WIDTH - 1 - x;
  }
  if (y + height >= DISP_HEIGHT - 1)
  {
    height = DISP_HEIGHT - 1 - y;
  }
  if (alpha == 255)
  {
    uint16_t pixel = epicfb_pack (red, green, blue);
    for (int v = y; v < y + height; v++)
    {
      for (int u = x; u < x + width; u++)
      {
        fb[epicfb_offset(u,v)] = pixel;
      }
    }
  }
  else
  {
    for (int v = y; v < y + height; v++)
    {
      for (int u = x; u < x + width; u++)
      {
        int o = epicfb_offset(u,v);
        fb[o] = epicfb_pack_blend (fb[o], red, green, blue, alpha);
      }
    }
  }
  epicfb_dirty ();
}

int  epicfb_menu_active   (void)
{
  return menu != NULL;
}

void epicfb_dispatch_events (void);

int epicfb_events ()
{
  epicfb_dispatch_events ();
  int ret = 0;

#if USE_SDL
  while (SDL_PollEvent(&event))
  {
    if (event.type == SDL_QUIT)
      exit(0);
  }

  const uint8_t *state = SDL_GetKeyboardState(NULL);
  int32_t new_state = 0;

  new_state |= state[SDL_SCANCODE_LEFT]  ? BUTTON_LEFT_BOTTOM : 0;
  new_state |= state[SDL_SCANCODE_RIGHT] ? BUTTON_RIGHT_BOTTOM : 0;
  new_state |= state[SDL_SCANCODE_UP]    ? BUTTON_RIGHT_TOP : 0;
  new_state |= state[SDL_SCANCODE_ESCAPE]? BUTTON_LEFT_TOP : 0;
#else
  int32_t new_state = epic_buttons_read
      (BUTTON_LEFT_BOTTOM|BUTTON_RIGHT_BOTTOM|BUTTON_RIGHT_TOP|BUTTON_LEFT_TOP);
#endif

  epicfb_state = new_state;

  if (epicfb_menu_active())
  {
    ret = epicfb_menu_events ();
  }

  return ret;
}

static inline int leftmost_bit_set (int number)
{
  int rshifts = 0;
  while (number != 0)
  {
    rshifts ++;
    number >>= 1;
  }
  return rshifts - 1;
}


static int press_time[4] = {0,};

int epicfb_event (int button, int type)
{
  if ((menu  && !in_menu_events) ||
      (menu == NULL && menu_was_active))
    return 0;

  int ret = 0;
  int button_no = leftmost_bit_set (button);

  switch (type)
  {
    case PRESSED:
      ret = ((epicfb_state & button) != 0);
      break;
    case PRESS_REPEAT:
      {
        int is_pressed   = (epicfb_state & button)  != 0;
        int got_pressed  = is_pressed && ((epicfb_prev_state & button) == 0);

        if (got_pressed ||
            (is_pressed &&
             (epicfb_ticks > press_time[button_no] + epicfb_press_repeat_delay)))
        {
          press_time[button_no] = epicfb_ticks;
          ret = 1;
        }
      }
      break;
    case PRESS:
    {
      int got_pressed = ((epicfb_state & button)  != 0) &&
                        ((epicfb_prev_state & button)  == 0);
      int got_released = ((epicfb_state & button)  == 0) &&
                        ((epicfb_prev_state & button)  != 0);

      if (prev_long_press_mask & button)
      {
         if (got_pressed)
         {
            press_time[button_no] = epicfb_ticks;
            ret = 0;
         }
         else
         {
            if (got_released)
            {
              if (epicfb_ticks < press_time[button_no] + epicfb_long_press_delay)
              {
                press_time[button_no] = 0;
                ret = 1;
              }
            }
         }
      } else
      {
         ret = got_pressed;
      }
    }
      break;
    case RELEASE:
      ret = ((epicfb_state & button)  == 0) &&
            ((epicfb_prev_state & button)  != 0);
      if (ret)
      {
         press_time[button_no] = 0;
      }
      break;
    case LONG_PRESS:
      long_press_mask |= button;
      if  (((epicfb_state & button) != 0) &&
            epicfb_ticks > press_time[button_no] + epicfb_long_press_delay &&
            press_time[button_no]
)
      {
        ret = 1;
        press_time[button_no] = 0;
      }
      break;
  }
  return ret;
}

void epicfb_flip (uint16_t *fb)
{
#if USE_SDL
  SDL_UpdateTexture(textureSDL,NULL,fb,DISP_WIDTH * sizeof(uint16_t));
  SDL_RenderClear(renderer);
  SDL_RenderCopy(renderer,textureSDL,NULL,NULL);
  SDL_RenderPresent(renderer);

#else
  epic_disp_open ();
  epic_disp_framebuffer ((void*)fb);
  epic_disp_close ();
#endif
}

#define UPNG_IMPLEMENTATION
#include "upng.h"

static upng_t *upng_cached = NULL;
static char *upng_cached_path = NULL;

static int epicfb_png_width = 0;
static int epicfb_png_height = 0;

void epicfb_png_last_dim (int *width,
                          int *height)
{
  if (width) *width = epicfb_png_width;
  if (height) *height = epicfb_png_height;
}

void epicfb_blit (const uint8_t *src,
                  int       srcw,
                  int       srch,
                  int       c,
                  uint16_t *dst,
                  int      x0,
                  int      y0,
                  int      w,
                  int      h,
                  uint8_t  opacity,
                  int      filter)
{
  int cc = c + (filter?10:0);

  if (!src && dst)
  {
    return;
  }

  int stride = srcw * c;

  /* make sure we have valid height if it is unspecified */
  if (h <= 0)
    {
      if (w > 0)
      {
        /* base height on requested width and aspect  */
        h = srch * w / srcw;
      }
      else
      {
        /* both unspecified, use image dims */
        h = srch;
        w = srcw;
      }
    }

  if (w <= 0)
  {
    /* only height was specified, base width on height and aspect  */
    w = h * srcw / srch;
  }

  int scale = 256 * h / srch;

  for (int y = y0; y < y0 + h; y++)
  {
    int src_offset = ((y-y0) * 256 / scale) * c * srcw;

    int dy = ((y-y0) * 65536 / scale) & 0xff;

    for (int x = x0; x < x0 + w; x++)
    {
      if (x >= 0 && y>=0 && x < DISP_WIDTH && y < DISP_HEIGHT)
      {
        int fb_offset  = epicfb_offset(x,y);
        int io = src_offset + ((x-x0) * 256 / scale) * c;
        uint8_t red = 0; uint8_t green= 0; uint8_t blue = 0;
        uint8_t alpha = opacity;
        int dx = 0;

        if (filter)
          dx= ((x-x0) * 65536 / scale) & 0xff;

        if (io >= 0 && io < (srcw * (srch-1) -1 ) * c)
        switch (cc)
        {
          /* nearest neighbor */

          case 2: alpha = src[io+1] * opacity / 256;
          case 1: red = green = blue = src[io];
             break;
          case 4: alpha = src[io+3] * opacity / 256;
          case 3: red = src[io];
                  green = src[io+1];
                  blue = src[io+2];
                  break;

         /* bilinear */

#define bilinear_src(offset, component) \
  lerp_u8(lerp_u8(src[offset+component], src[offset+component+c], dx),\
  lerp_u8(src[offset+component+stride], src[offset+component+c+stride], dx), dy)

          case 12: alpha = bilinear_src(io, 1) * opacity / 256;
          case 11: red = green = blue = bilinear_src(io, 0);
             break;
          case 14: alpha = bilinear_src(io, 3) * opacity / 256;
          case 13: red = bilinear_src(io, 0);
                   green = bilinear_src(io, 1);
                   blue = bilinear_src(io, 2);
                   break;

        }
        if (alpha == 255)
          dst[fb_offset] = epicfb_pack (red, green, blue);
        else
          dst[fb_offset] = epicfb_pack_blend (dst[fb_offset], red, green, blue, alpha);
      }
    }
  }
}


void epicfb_png (uint16_t *pixels,
                 int      x0,
                 int      y0,
                 int      w,
                 int      h,
                 uint8_t  opacity,
                 int      filter,
                 int      cache,
                 const char *path)
{
  int pngw;
  int pngh;

  upng_t *upng = NULL;

  if (!cache && upng_cached)
  {
    upng_free (upng_cached);
    upng_cached = NULL;
  }

  if (cache && upng_cached_path && !strcmp (upng_cached_path, path))
  {
    upng = upng_cached;
    upng_cached = NULL;
    free (upng_cached_path);
    upng_cached_path = NULL;
  }
  else
  {
    if (upng_cached)
    {
      upng_free (upng_cached);
      upng_cached = NULL;
      if (upng_cached_path)
        free (upng_cached_path);
      upng_cached_path = NULL;
    }
  }

  if (!upng){
    upng = upng_new_from_file (path);
    if (upng == NULL)
    {
    //  epicfb_print ("failed loading png\n");
      return;
    }
    upng_header (upng);
  }

  upng_decode (upng);
  pngw = upng_get_width (upng);
  pngh = upng_get_height (upng);
  int c = upng_get_components (upng);

  epicfb_png_width = pngw;
  epicfb_png_height = pngh;

  //printf ("%ix%i\n", w, h);
  const uint8_t *buffer = upng_get_buffer (upng);

  epicfb_blit (buffer, pngw, pngh, c,
               pixels,
               x0,
               y0,
               w,
               h,
               opacity,
               filter);

  if (cache)
  {
    upng_cached = upng;
    upng_cached_path = strdup (path);
  }
  else
  {
    upng_free (upng);
  }
}

typedef struct _EpicTimeEventFull EpicTimeEventFull;


struct _EpicTimeEventFull {
  int     at_ticks;
  MenuCB  cb;
  void   *cb_data;
  MenuCB  finalize;
  EpicTimeEventFull *next;
};

static EpicTimeEventFull *time_events = NULL;
static TimeEvent *script_events = NULL;
static int        script_n_events = 0;
static int        script_start_time = 0;
static int        script_iter = 0;

void epicfb_queue_script (int ms, TimeEvent *events, int n_events)
{
  script_start_time = epicfb_ticks + ms;
  script_events = events;
  script_n_events = n_events;
}

void epicfb_iterate_script (void)
{
  if (script_events && script_iter >= 0)
    if (epicfb_ticks > (script_events[script_iter].time * 1000) +
                       script_start_time)
    {
      if (script_events[script_iter].cb)
      {
        ((MenuCB)(script_events[script_iter].cb)) (script_events[script_iter].cb_data);
      }
      script_iter++;
      if (script_iter >= script_n_events)
        script_iter = -1;
    }
}

void epicfb_queue_cb (int ms, MenuCB cb, void *cb_data, MenuCB finalize)
{
  EpicTimeEventFull *event = calloc (sizeof (EpicTimeEventFull), 1);

  event->at_ticks = epicfb_ticks + ms;
  event->cb = cb;
  event->cb_data = cb_data;
  event->finalize = finalize;

  if (time_events)
  {
    EpicTimeEventFull *i = time_events;

    while (i && i->next && i->at_ticks < event->at_ticks) i = i->next;

    event->next = i->next;
    i->next = event;
  }
  else
  {
    time_events = event;
  }
}

void epicfb_dispatch_events (void)
{
  epicfb_iterate_script ();
  again:
  {
    EpicTimeEventFull *i = time_events;
    EpicTimeEventFull *prev = NULL;
    while (i)
    {
      if (i->at_ticks <= epicfb_ticks)
      {
        if (prev)
          prev->next = i->next;
        else
          time_events = i->next;
        i->cb (i->cb_data);
        if (i->finalize)
          i->finalize (i->cb_data);
        free (i);
        goto again;
      }
      prev = i;
      i = i->next;
    }
  }
}

void epicfb_dirty (void)
{
  frame_changed ++;
}

int epicfb_isdirty (void)
{
  return (frame_changed != 0);
}

void epicfb_sleep (int ms)
{
  if (ms < 0)
    return;
#if USE_SDL
  SDL_Delay (ms);
#else
  int prev_time = epicfb_ticks_live ();
  while (epicfb_ticks_live () < prev_time + ms);
#endif
}

uint8_t *epicfb_file_load(const char *path, int *data_size)
{
#if USE_SDL
  FILE *file;
  long  size;
  long  remaining;
  char *buffer;

  file = fopen (path,"rb");

  if (!file)
    return NULL;

  {
    fseek (file, 0, SEEK_END);
    size = remaining = ftell (file);
    rewind (file);
    buffer = malloc(size);
  }

  if (!buffer)
    {
      fclose(file);
      return NULL;
    }
  remaining -= fread (buffer, 1, remaining, file);
  if (remaining)
    {
      fclose (file);
      free (buffer);
      return NULL;
    }
  fclose (file);
#else
  uint8_t *buffer;
  int fd;
  size_t size;

  fd = epic_file_open(path, "rb");
  if (fd < 0)
  {
    return NULL;
  }

  epic_file_seek(fd, 0, SEEK_END);
  size = epic_file_tell(fd);
  epic_file_seek(fd, 0, SEEK_SET);
  buffer = malloc(size);
  if (!buffer)
  {
    epic_file_close(fd);
    return NULL;
  }

  size_t n = epic_file_read(fd, buffer, size);
  if (n != size)
  {
    free (buffer);
    return NULL;
  }
  epic_file_close(fd);
#endif
  if (data_size)
    *data_size = size;
  return buffer;
}

char *epicfb_path_no (int no, const char **extensions)
{
#if USE_SDL
  int count = 0;
  char cwdbuf[512];
  getcwd (cwdbuf, sizeof(cwdbuf));
  DIR  *dir = opendir (cwdbuf);
  struct dirent *entry;
  if (!dir)
    return NULL;

  entry = readdir (dir);
  while (entry)
  {
    const char *name = entry->d_name;

    int match = 0;
    const char *last_dot = strrchr (name, '.');

    for (const char **e = extensions; *e; e++)
      if (last_dot && !strcmp (last_dot, *e))
        match = 1;

    if (match)
    {
      if (no == count)
      {
        char *ret = strdup (name);
        closedir (dir);
        return ret;
      }
      count ++;
    }
    entry = readdir (dir);
  }
  closedir (dir);
#else
  int count = 0;
  int fd = epic_file_opendir("/");
  struct epic_stat entry;
  if (fd)
  for (;;) {
    epic_file_readdir(fd, &entry);
    if (entry.type == EPICSTAT_NONE)
    {
      epic_file_close (fd);
      return NULL;
    }
    const char *name = entry.name;

    int match = 0;
    const char *last_dot = strrchr (name, '.');
    for (const char **e = extensions; *e; e++)
      if (last_dot && !strcmp (last_dot, *e))
        match = 1;

    if (match)
    {
      if (no == count)
      {
        epic_file_close (fd);
        return strdup (name);
      }
      count ++;
    }
  }
  epic_file_close (fd);
#endif
  return NULL;
}
