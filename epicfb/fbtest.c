
#include "epicfb.h"
#include <stdio.h>

static uint16_t pixels[DISP_WIDTH * DISP_HEIGHT]; /* our frame buffer */

#define printf(foo...) \
   do{ char str[512];\
       snprintf(str, 512, foo);\
       epicfb_print (str);\
   }while(0)

void cb_close_menu (void *unused)
{
  epicfb_set_menu (NULL);
}

static void cb_show_ascii (const char *menu_entry)
{
  for (int c = ' '; c < 126; c++)
    printf ("%c", c);
  printf ("\n");
}

static void cb_vibrate (void *data)
{
#if USE_SDL==0
  epic_vibra_vibrate (100);
#else
  fprintf (stderr, "brrrr\n");
#endif
}

static void cb_vibrate_in_5 (const char *menu_entry)
{
  epicfb_queue_cb (5000, cb_vibrate, NULL, NULL);
}

static void cb_vibrate_in_10 (const char *menu_entry)
{
  epicfb_queue_cb (10000, cb_vibrate, NULL, NULL);
}

static Menuitem test_menu[]={
  { MENU_ACTION_MENUTITLE,  "epicfb test"},
  { MENU_ACTION_CB,         "close menu",          cb_close_menu, NULL},
  { MENU_ACTION_INTEGER,    "font size",          &epicfb_font_size},
  { MENU_ACTION_INTEGER,    "line spacing",       &epicfb_line_spacing_percent},
  { MENU_ACTION_INTEGER,    "long press ms",      &epicfb_long_press_delay},
  { MENU_ACTION_INTEGER,    "press rep ms",       &epicfb_press_repeat_delay},
  { MENU_ACTION_INTEGER,    "menu bg level",      &epicfb_menu_bg_brightness},
  { MENU_ACTION_CB,         "print ASCII",        cb_show_ascii},
#if USE_SDL==0
  { MENU_ACTION_CB,         "enable usbstorage",  epic_usb_storage},
  { MENU_ACTION_CB,         "disable usbstorage", epic_usb_cdcacm},
#endif
  { MENU_ACTION_CB,         "vibrate",            cb_vibrate},
  { MENU_ACTION_CB,         "vibrate in 5",       cb_vibrate_in_5},
  { MENU_ACTION_CB,         "vibrate in 10",      cb_vibrate_in_10},
  { MENU_ACTION_TERMINAL}
};

int main(void)
{
  epicfb_init ();

  for (;;)
  {
    epicfb_start_frame ();
    epicfb_fill (pixels, epicfb_frag_noise, 0, NULL);
    epicfb_render_fps (pixels);
    epicfb_update (pixels, 0);

    epicfb_events ();

    if (epicfb_event (BUTTON_LEFT, PRESS))
    {
      printf ("left press\n");
    }

    if (epicfb_event (BUTTON_LEFT, LONG_PRESS))
    {
      printf ("left long press\n");
    }

    if (epicfb_event (BUTTON_LEFT, RELEASE))
    {
      printf ("left release\n");
    }

    if (epicfb_event (BUTTON_RIGHT, PRESS_REPEAT))
    {
      printf ("right press\n");
    }

    if (epicfb_event (BUTTON_SELECT, PRESS))
    {
      printf ("select press\n");
      epicfb_set_menu (test_menu);
    }

    if (epicfb_event (BUTTON_QUIT, PRESS))
    {
      printf ("quit press\n");
    }
    if (epicfb_event (BUTTON_QUIT, RELEASE))
    {
      printf ("quit release\n");
    }

    if (epicfb_event (BUTTON_RIGHT, RELEASE))
    {
      printf ("right release\n");
    }

  }

  return 0;
}

